﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class PathInformation
    {
        public string Name { get; }

        public PathInformation(string name)
        {
            Name = name;
        }

    }
}
