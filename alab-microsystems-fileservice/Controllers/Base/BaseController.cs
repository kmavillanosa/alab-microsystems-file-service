﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace alab_microsystems_fileservice
{
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/file-service/")]
    public class BaseController : Controller
    {
        private IMediator _Mediator;
        protected IMediator Mediator
        {
            get
            {
                return _Mediator ?? (_Mediator = HttpContext.RequestServices.GetService<IMediator>());
            }
        }

        private IMapper _Mapper;
        protected IMapper Mapper
        {
            get
            {
                return _Mapper ?? (_Mapper = HttpContext.RequestServices.GetService<IMapper>());
            }
        }
    }
}
