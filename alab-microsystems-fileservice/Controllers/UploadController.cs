﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace alab_microsystems_fileservice.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    
    public class UploadController : BaseController
    {

        readonly IUploadService FileServiceProvider;


        public UploadController(IUploadService fileServiceProvider)
        {
            FileServiceProvider = fileServiceProvider;
        }


        /// <summary>
        /// Upload single file of any file extension
        /// </summary>
        /// <param name="appcode"></param>
        /// <param name="group"></param>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("upload/{appcode}/{group}/{id}")]
        public async Task<IActionResult> Upload(string appcode, string group , string id,  IFormFile file)
        {
            try
            {
                var result = await FileServiceProvider.Upload(appcode, group, id, file);
                return Ok(result);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
          
        }

        /// <summary>
        /// Upload avatar / profile picture
        /// </summary>
        /// <param name="appcode"></param>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("upload/avatar/{appcode}/{id}")]
        public async Task<IActionResult> UploadAvatar(string appcode, string id, IFormFile file)
        {
            try
            {
                var result = await FileServiceProvider.UploadAvatar(appcode, id, file);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        /// <summary>
        ///  Upload files on batch categorize by group of any file extension
        /// </summary>
        /// <param name="appcode"></param>
        /// <param name="group"></param>
        /// <param name="id"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost("upload/batch/{appcode}/{group}/{id}")]
        public async Task<IActionResult> UploadBatch(string appcode, string group, string id, [FromForm(Name = "files")] List<IFormFile> files)
        {
            try
            {
                var result = await FileServiceProvider.UploadBatch(appcode, group, id, files);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
