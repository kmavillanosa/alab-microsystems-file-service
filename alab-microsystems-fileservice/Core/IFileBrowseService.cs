﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public interface IFileBrowseService
    {
        public List<PathInformation> GetFiles(string appcode, string id);
    }

    public class FileBrowseService : IFileBrowseService
    {
        private string _basePath;


        public List<PathInformation> GetFiles(string appcode, string id)
        {

            DirectoryInfo di = new DirectoryInfo(_basePath);

            var list = di.GetDirectories().ToList();

            return list.Select(x => new PathInformation(x.Name)).ToList();
        }

        public FileBrowseService UsingRootPath(string path)
        {
            _basePath = path;
            return this;
        }

    }
}
