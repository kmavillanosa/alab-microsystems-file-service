﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public interface IUploadService
    {
        Task<UploadResult> UploadAvatar(string code, string id, IFormFile files);
        Task<UploadResult> Upload(string code, string groupId, string id, IFormFile file);
        Task<UploadResult> UploadBatch(string code, string groupId, string id, List<IFormFile> files);
    }

    public class UploadService : IUploadService
    {
        private string _basePath;

        public async Task<UploadResult> Upload(string code, string groupId, string id, IFormFile file)
        {
            var path_to_save = Path.Combine(_basePath, code, groupId, id);

            if (!Directory.Exists(path_to_save))
            {
                Directory.CreateDirectory(path_to_save);
            }

            var filePath = Path.Combine(path_to_save, file.FileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            return await Task.FromResult(new UploadResult(1, 0, DateTime.Now));
        }

        public async Task<UploadResult> UploadBatch(string code, string groupId, string id, List<IFormFile> files)
        {
            int success = 0;
            int failed = 0;

            var path_to_save = Path.Combine(_basePath, code, groupId, id);

            if (!Directory.Exists(path_to_save))
            {
                Directory.CreateDirectory(path_to_save);
            }

            foreach(var file in files)
            {
                try
                {
                    var filePath = Path.Combine(path_to_save, file.FileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                        success++;
                    }
                }
                catch
                {
                    failed++;
                }
            

            }
            return await Task.FromResult(new UploadResult(success, failed, DateTime.Now));
        }

        public async Task<UploadResult> UploadAvatar(string code, string id, IFormFile file)
        {

            var file_extension = Path.GetExtension(file.FileName);

            List<string> AllowedExtensions = new List<string>(new string[] { ".png", ".jpg", ".jpeg" });

            if (!AllowedExtensions.Contains(file_extension))
            {
                throw new Exception("Invalid image format");
            }

            int success = 0;
            int failed = 0;

            var path_to_save = Path.Combine(_basePath, code, "profile-picture", id);

            if (!Directory.Exists(path_to_save))
            {
                Directory.CreateDirectory(path_to_save);
            }
            
            var filePath = Path.Combine(path_to_save, $"default.png");
            using (var fileStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                try
                {

                    await file.CopyToAsync(fileStream);
                    success++;
                }
                catch
                {
                    failed++;
                }
            }
            return await Task.FromResult(new UploadResult(success, failed, DateTime.Now));
        }



        public UploadService UsingRootPath(string path)
        {
            _basePath = path;
            return this;
        }
    }
}
