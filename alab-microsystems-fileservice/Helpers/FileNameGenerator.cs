﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class FileNameGenerator
    {
        public static string GenerateFileName(params string[] names)
        {
            var data = names.ToHashSet();
            return data.ToString();
        }
    }
}
